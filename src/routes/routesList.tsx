import { Register, MainPage } from "modules"

export const routesList = [
  {
    path: "/register",
    children: [
      {
        index: true,
        element: <Register />,
      },
    ],
  },
  {
    path: "*",
    element: <MainPage />,
  },
]
