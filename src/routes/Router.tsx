import { BrowserRouter, useRoutes } from "react-router-dom"
import { routesList } from "./routesList"

const RoutesList = () => {
  const routes = useRoutes(routesList)
  return routes
}

export const Router = () => {
  return (
    <BrowserRouter>
      <RoutesList />
    </BrowserRouter>
  )
}
