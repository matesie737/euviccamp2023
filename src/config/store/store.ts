import { configureStore, combineReducers } from "@reduxjs/toolkit"
import { setupListeners } from "@reduxjs/toolkit/dist/query"
import { authReducer } from "./auth"

const rootReducer = combineReducers({
  authReducer,
})

const setup = () => {
  const store = configureStore({
    reducer: rootReducer,
    middleware: (getDefaultMiddleware) =>
      getDefaultMiddleware({
        serializableCheck: false,
      }),
  })
  setupListeners(store.dispatch)
  return store
}

export const store = setup()

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch
