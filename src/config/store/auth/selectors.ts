import { createSelector } from '@reduxjs/toolkit'

import { RootState } from 'config'

export const getAuth = (state: RootState) => state.authReducer

export const getAuthErrorMessage = createSelector(getAuth, state => state.error)
export const getAuthLoadingStatus = createSelector(getAuth, state => state.loading)
