import { createReducer } from '@reduxjs/toolkit'

import { LoadingStatus, Rejected, State } from 'types/api.types'

import { register } from './actions'

const initialState: State = {
  loading: LoadingStatus.Idle,
  error: null,
}

export default createReducer(initialState, builder =>
  builder
    .addCase(register.pending, state => {
      state.loading = LoadingStatus.Pending
      state.error = initialState.error
    })
    .addCase(register.fulfilled, state => {
      state.loading = LoadingStatus.Succeeded
      state.error = null
    })
    .addCase(register.rejected, (state, action) => {
      state.loading = LoadingStatus.Failed
      const act = action as Rejected
      state.error = act.payload ? act.payload.message : act.error.message
    })
)
