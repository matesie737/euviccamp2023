import { createAsyncThunk } from "@reduxjs/toolkit"
import { RegisterPayLoad } from "types/api.types"

export const register = createAsyncThunk(
  "auth/register",
  async (credentails: RegisterPayLoad, thunkAPI) => {
    try {
      console.log(credentails)
      const sleep = (ms: number) => new Promise((resolve) => setTimeout(resolve, ms))
      await sleep(2000)
      throw new Error("Usługa tymczasowo nieaktywna")
    } catch (err) {
      return thunkAPI.rejectWithValue(err)
    }
  }
)
