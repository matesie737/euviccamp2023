import {
  styled,
  TextField as BaseTextField,
  Button as BaseButton,
  Box as BaseBox,
} from "@mui/material"
import { LoadingButton as BaseLoadingButton } from "@mui/lab"

export const Column = styled("div")({
  position: "absolute",
  top: "0",
  height: "100vh",
  width: "50vw",
})

export const ImageContainer = styled("div")({
  height: "100%",
  width: "100%",
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
})

export const Image = styled("img")({
  height: "300px",
})

export const Form = styled("form")({
  display: "flex",
  rowGap: "20px",
  flexDirection: "column",
  justifyContent: "center",
  alignItems: "center",
  width: "80%",
})

export const Container = styled("div")({
  display: "flex",
  flexDirection: "column",
  justifyContent: "center",
  alignItems: "center",
  height: "100vh",
})

export const FormContainer = styled("div")({
  display: "flex",
  flexDirection: "column",
  justifyContent: "center",
  alignItems: "center",
  width: "400px",
  padding: "20px 0px",
  backgroundColor: "#FFFFFF",
})

export const TextField = styled(BaseTextField)`
  .MuiInputBase-root {
    background-color: #ffffff;
  }
`

export const Button = styled(BaseButton)({
  color: "#222222",
  backgroundColor: "#F5F5F5",
  ":hover": {
    backgroundColor: "#C4C4C4",
  },
})
export const LoadingButton = styled(BaseLoadingButton)({
  color: "#222222",
  backgroundColor: "#F5F5F5",
  ":hover": {
    backgroundColor: "#C4C4C4",
  },
})

export const ButtonContainer = styled("div")({
  display: "flex",
  flexDirection: "row",
  gap: "20px",
  width: "100%",
  marginBottom: '10px'
})

export const BottomInformation = styled("p")({
  height: "13px",
  margin: "0 0",
  fontSize: "13px",
  color: "#808080",
})

export const ModalBox = styled(BaseBox)({
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 300,
  minHeight: 350,
  display: "flex",
  flexDirection: "column",
  justifyContent: "space-evenly",
  alignItems: "center",
})

export const SummaryBox = styled(BaseBox)({
  display: "flex",
  flexDirection: "column",
  width: "90%",
  marginBottom: "20px",
  gap: "10px",
})
