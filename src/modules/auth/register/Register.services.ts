import { useForm } from "react-hook-form"

import { yupResolver } from "@hookform/resolvers/yup"

import { useRegisterSchema } from "./Register.validator"
import { Role } from "types/api.types"
import { register, useAppDispatch } from "config"

export enum RegisterFormFields {
  Email = "email",
  Password = "password",
  Nip = "nip",
  ConfirmPassword = "confirmpassword",
  Phone = "phone",
  Role = "role",
}

export interface RegisterFormValues {
  [RegisterFormFields.Email]: string
  [RegisterFormFields.Password]: string
  [RegisterFormFields.Nip]: string
  [RegisterFormFields.ConfirmPassword]: string
  [RegisterFormFields.Phone]: string
  [RegisterFormFields.Role]: Role
}

export const defaultValues: RegisterFormValues = {
  [RegisterFormFields.Email]: "",
  [RegisterFormFields.Password]: "",
  [RegisterFormFields.Nip]: "",
  [RegisterFormFields.ConfirmPassword]: "",
  [RegisterFormFields.Phone]: "",
  [RegisterFormFields.Role]: Role.Administrator,
}

const useOnSubmit = (onSuccess: () => void) => {
  const dispatch = useAppDispatch()
  const onSubmit = async (data: RegisterFormValues) => {
    const res = await dispatch(
      register({
        email: data[RegisterFormFields.Email],
        password: data[RegisterFormFields.Password],
        nip: data[RegisterFormFields.Nip],
        confirmpassword: data[RegisterFormFields.ConfirmPassword],
        phone: data[RegisterFormFields.Phone],
        role: data[RegisterFormFields.Role],
      })
    )
    console.log(res)
    //onSuccess()
  }
  return onSubmit
}

export const useFormProps = () => {
  const schema: any = useRegisterSchema()
  const methods = useForm<RegisterFormValues>({
    defaultValues: defaultValues,
    resolver: yupResolver(schema),
    mode: "onChange",
    reValidateMode: "onChange",
  })
  const onSubmit = useOnSubmit(methods.reset)

  return { ...methods, onSubmit }
}
