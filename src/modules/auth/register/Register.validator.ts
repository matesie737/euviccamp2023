import * as yup from "yup"
import "yup-phone-lite"
import YupPassword from "yup-password"
YupPassword(yup)

export const useRegisterSchema = () => {
  const schema = yup
    .object({
      email: yup
        .string()
        .required("Pole wymagane")
        .email("Proszę wprowadzić poprawny adres Email"),
      password: yup
        .string()
        .required("Pole wymagane")
        .min(8, "Hasło musi składać się z co najmniej 8 znaków")
        .max(32, "Hasło jest za długie")
        .minLowercase(1, "Hasło musi zawierać conajmniej jedną małą literę")
        .minUppercase(1, "Hasło musi zawierać conajmniej jedną dużą literę")
        .minNumbers(1, "Hasło musi zawierać conajmniej jedną cyfrę")
        .minSymbols(1, "Hasło musi zawierać conajmniej jeden znak specjalny"),
      nip: yup.string().length(10, "Nip musi składać sięz 10 znaków").required("Pole wymagane"),
      confirmpassword: yup
        .string()
        .oneOf([yup.ref("password")], "Hasła muszą być takie same")
        .required("Pole wymagane"),
      phone: yup.string().phone("PL", "Proszę wprowadzić poprawny numer telefonu"),
    })
    .required()

  return schema
}
