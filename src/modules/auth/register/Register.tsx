import { RegisterFormFields, useFormProps } from "./Register.services"
import {
  FormContainer,
  Form,
  Container,
  TextField,
  Button,
  ButtonContainer,
  BottomInformation,
  Column,
  ImageContainer,
  Image,
  ModalBox,
  SummaryBox,
  LoadingButton,
} from "./Register.styles"
import { Visibility, VisibilityOff } from "@mui/icons-material"
import {
  Box,
  MenuItem,
  Modal,
  Typography,
  IconButton,
  InputAdornment,
} from "@mui/material"
import { logo } from "assets"
import { useState } from "react"
import { Role, isPending } from "types/api.types"
import { getAuthLoadingStatus, getAuthErrorMessage } from "config"
import { useSelector } from "react-redux"
import { useNavigate } from "react-router"

const Register = () => {
  const { register, handleSubmit, onSubmit, formState, getValues, trigger } =
    useFormProps()

  const { errors, isValid } = formState
  const navigate = useNavigate()

  const [modalOpen, setModalOpen] = useState(false)
  const handleOpen = () => setModalOpen(true)
  const handleClose = () => setModalOpen(false)

  const [showConfirmPassword, setShowConfirmPassword] = useState(false)
  const handleClickShowConfirmPassword = () =>
    setShowConfirmPassword(!showConfirmPassword)
  const handleMouseDownConfirmPassword = () =>
    setShowConfirmPassword(!showConfirmPassword)

  const [showPassword, setShowPassword] = useState(false)
  const handleClickShowPassword = () => setShowPassword(!showPassword)
  const handleMouseDownPassword = () => setShowPassword(!showPassword)

  const loading = useSelector(getAuthLoadingStatus)
  const error = useSelector(getAuthErrorMessage)

  return (
    <>
      <Column>
        <ImageContainer>
          <Image src={logo}></Image>
        </ImageContainer>
      </Column>
      <Column sx={{ right: "0" }}>
        <Container>
          <FormContainer>
            <Form
              onSubmit={handleSubmit(onSubmit)}
              id="registerForm"
              style={{ gap: "10px" }}
            >
              <Box sx={{ marginBottom: "10px", color: "#2371af" }}>
                {"Rejestracja"}
              </Box>
              <TextField
                {...register(RegisterFormFields.Email)}
                label={"Email*"}
                error={!!errors.email}
                helperText={errors.email ? errors.email.message : ""}
                fullWidth
                size="small"
              />
              <TextField
                {...register(RegisterFormFields.Password)}
                label={"Hasło*"}
                error={!!errors.password}
                helperText={errors.password ? errors.password.message : ""}
                fullWidth
                color="secondary"
                size="small"
                type={showPassword ? "text" : "password"}
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="end">
                      <IconButton
                        aria-label="toggle password visibility"
                        onClick={handleClickShowPassword}
                        onMouseDown={handleMouseDownPassword}
                      >
                        {showPassword ? <Visibility /> : <VisibilityOff />}
                      </IconButton>
                    </InputAdornment>
                  ),
                }}
              />
              <TextField
                {...register(RegisterFormFields.Nip)}
                label={"NIP*"}
                error={!!errors.nip}
                helperText={errors.nip ? errors.nip.message : ""}
                fullWidth
                color="secondary"
                size="small"
              />
              <TextField
                {...register(RegisterFormFields.ConfirmPassword)}
                label={"Powtórz hasło*"}
                error={!!errors.confirmpassword}
                helperText={
                  errors.confirmpassword ? errors.confirmpassword.message : ""
                }
                fullWidth
                color="secondary"
                size="small"
                type={showConfirmPassword ? "text" : "password"}
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="end">
                      <IconButton
                        onClick={handleClickShowConfirmPassword}
                        onMouseDown={handleMouseDownConfirmPassword}
                      >
                        {showConfirmPassword ? (
                          <Visibility />
                        ) : (
                          <VisibilityOff />
                        )}
                      </IconButton>
                    </InputAdornment>
                  ),
                }}
              />
              <TextField
                {...register(RegisterFormFields.Phone)}
                label={"Telefon"}
                error={!!errors.phone}
                helperText={errors.phone ? errors.phone.message : ""}
                fullWidth
                color="secondary"
                size="small"
              />
              <TextField
                {...register(RegisterFormFields.Role)}
                select
                label={"Rola*"}
                error={!!errors.role}
                fullWidth
                color="secondary"
                size="small"
                defaultValue={Role.Administrator}
              >
                {(Object.keys(Role) as (keyof typeof Role)[]).map(
                  (key, index) => {
                    return (
                      <MenuItem key={index} value={Role[key]}>
                        {key}
                      </MenuItem>
                    )
                  }
                )}
              </TextField>
              <ButtonContainer>
                <Button
                  variant="contained"
                  onClick={() => navigate("/")}
                  fullWidth
                >
                  {"Anuluj"}
                </Button>
                <Button
                  onClick={() => (isValid ? handleOpen() : trigger())}
                  variant="contained"
                  fullWidth
                >
                  {"Podsumowanie"}
                </Button>
              </ButtonContainer>
              <BottomInformation>{"* - pole wymagane"}</BottomInformation>

              <Modal open={modalOpen} onClose={handleClose}>
                <ModalBox
                  sx={{
                    boxShadow: 24,
                    bgcolor: "#FFFFFF",
                    p: 4,
                  }}
                >
                  <Typography
                    variant="h6"
                    component="h2"
                    sx={{ marginBottom: "20px" }}
                  >
                    {"Podsumowanie"}
                  </Typography>
                  <SummaryBox>
                    <div>{"Email: "}</div>
                    <Typography
                      sx={{ color: "#808080", wordWrap: "break-word" }}
                    >
                      {getValues(RegisterFormFields.Email)}
                    </Typography>
                    <div>{"NIP: "}</div>
                    <Typography sx={{ color: "#808080" }}>
                      {getValues(RegisterFormFields.Nip)}
                    </Typography>
                    <div>{"Telefon: "}</div>
                    <Typography sx={{ color: "#808080" }}>
                      {getValues(RegisterFormFields.Phone)}
                    </Typography>
                    <div>{"Rola: "}</div>
                    <Typography sx={{ color: "#808080" }}>
                      {getValues(RegisterFormFields.Role)}
                    </Typography>
                  </SummaryBox>
                  <ButtonContainer>
                    <Button fullWidth onClick={handleClose}>
                      {"Edytuj"}
                    </Button>
                    <LoadingButton
                      fullWidth
                      type="submit"
                      loading={isPending(loading)}
                      form="registerForm"
                    >
                      {"Zarejestruj"}
                    </LoadingButton>
                  </ButtonContainer>
                  {error && (
                    <Typography sx={{ color: "#C24641" }}>{error}</Typography>
                  )}
                </ModalBox>
              </Modal>
            </Form>
          </FormContainer>
        </Container>
      </Column>
    </>
  )
}

export default Register
