import { Button } from "@mui/material"
import { Box } from "@mui/system"
import { useNavigate } from "react-router"

const MainPage = () => {
    const navigate = useNavigate()
  return (
    <Box
      sx={{
        width: "100vw",
        height: "100vh",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <Button onClick={()=>navigate('/register')}>Przejdz do rejestracji</Button>
    </Box>
  )
}

export default MainPage
